!
!  OX/RECORD. Record a name and its line numbers.
!
!  Copyright © 2015 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog

!  PLACE. A place where a name appears in a source file. It appears on the line
!  number NUMBER with the ROLEs in the set ROLES. PLACEs are linked into linear
!  chains in ascending order of their NUMBERs through their NEXT slots.

  place :−
   (tuple
     int number,
     var set roles,
     var ref place next)

!  MAKE PLACE. Return a new PLACE that holds NUMBER, and a set of ROLEs holding
!  only ROLE.

  makePlace :−
   (proc (int number, int role) ref place:
    (for var place this in making(place)
     do this.number := number
        this.roles  := makeSet(role)
        this.next   := nil))

!  TREE. A node in an unbalanced binary search tree. NAME is the key. FIRST and
!  LAST are the values associated with NAME: they're respectively the first and
!  last PLACEs in a singly linked chain. LEFT is a subtree whose NAMEs are less
!  than NAME, and RIGHT is a subtree whose NAMEs are greater than NAME.

  tree :−
   (tuple
     string name,
     var ref place first,
     var ref place last,
     var ref tree left,
     var ref tree right)

!  MAKE TREE. Return a TREE with NAME, and a single PLACE that holds NUMBER and
!  a set of ROLEs containing ROLE. If arguments are missing, then return a TREE
!  with an empty string as its NAME and no PLACE.

  makeTree :−
   (alt
    (form () ref tree:
      makingTree(ϵ, nil)),
    (form (string name, int number, int role) ref tree:
      makingTree(copy(name), makePlace(number, role))))

!  MAKING TREE. Do all the work for MAKE TREE.

  makingTree :−
   (proc (string name, ref place where) ref tree:
    (for var tree this in making(tree)
     do this.name  := name
        this.first := where
        this.last  := where
        this.left  := nil
        this.right := nil))

!  RECORD NAME. Add NAME, along with its line NUMBER and its ROLE on that line,
!  to the search tree ROOT. NAME defaults to a string with U+2B1A for a missing
!  name.

  recordName :−
   (alt
    (form (int number, int role) void:
      recordingName(''⬚'', number, role)),
    (form (var buffer(maxNameLength) name, int number, int role) void:
      recordingName(name{string}, number, role)),
    (form (string name, int number, int role) void:
      recordingName(name, number, role)))

!  RECORDING NAME. Do all the work for RECORD NAME.

  ref tree root :− makeTree()

  recordingName :−
   (proc (string name, int number, int role) void:
    (with var ref tree subtree :− root
     do (while
         (with int test :− comp(name, subtree↑.name)
          do (if test < 0
              then (if subtree↑.left = nil
                    then subtree↑.left := makeTree(name, number, role)
                         false
                    else subtree := subtree↑.left
                         true)
              else if test > 0
                   then (if subtree↑.right = nil
                         then subtree↑.right := makeTree(name, number, role)
                              false
                         else subtree := subtree↑.right
                              true)
                   else (if subtree↑.last↑.number = number
                         then subtree↑.last↑.roles ∪= makeSet(role)
                         else (with ref place next :− makePlace(number, role)
                               do subtree↑.last↑.next := next
                                  subtree↑.last := next))
                        false)))))
)
