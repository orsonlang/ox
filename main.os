!
!  OX/MAIN. Main program.
!
!  Copyright © 2015 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog
  string me           :− ''Ox''      !  Name of this program.
  string orsonPrelude :− ''.op''     !  Orson prelude file suffix.
  string orsonSource  :− ''.os''     !  Orson source file suffix.
  string version      :− ''0.9999''  !  Version of this program.

!  MAIN. Main program. Visit every argument on the command line, and do what it
!  says. Then write a cross reference table to OUTPUT.

  main :−
   (form () void:
    (for char option, string value in command(''alvz'', '' cdn'')
     do (case option
         of ' ': readFile(value)
            'a': asciiing := true
            'c': setHowMany(columnsPerName,  'c', value, 1)
            'd': setHowMany(digitsPerNumber, 'd', value, 1)
            'l': roleing := false
            'n': setHowMany(numbersPerLine,  'n', value, 0)
            'v': writeVersion()
            'z': accenting := true))
    reportNames())

!  READ FILE. Cross-reference an Orson source program whose pathname is PATH.

  readFile :−
   (form (string path) void:
    (for bool ok, string path' in canonical(path)
     do (if ok
         then (if isEnd(path', orsonPrelude) ∨ isEnd(path', orsonSource)
               then (if open(source, path', ''r'')
                     then sourceLine := 0
                          sourcePath := path'
                          sourceState := 0
                          nextChar()
                          nextToken()
                          nextProgram()
                          write(eop)
                          (if ¬ close(source)
                           then fail(''Cannot close '%s'.'': path'))
                     else fail(''Cannot open '%s'.'': path'))
               else fail(''Unexpected suffix in '%s'.'': path'))
         else fail(''Cannot open '%s'.'': path))))

!  SET HOW MANY. Set NUMBER to DIGITS from the option CH.

  setHowMany :−
   (proc (var int number, char ch, string digits, int min) void:
    (for bool ok, int howMany in convert(int, digits)
     do (if ok
         then (if howMany ≥ min
               then number := howMany
               else fail(''-%c must be at least %i.'': ch, min))
         else fail(''-%c must be an integer.'': ch))))

!  WRITE VERSION. Write a short string describing this program.

  writeVersion :−
   (form () void:
     writeln(errput, me & '', version '' & version & '.'))

!  BEGIN. Do it.

  begin :− main()
)
