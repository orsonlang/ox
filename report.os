!
!  OX/REPORT. Write names and their line numbers.
!
!  Copyright © 2015 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog

!  REPORT NAMES. Write names in lexicographic order. Following each name, write
!  a list of line numbers and roles. If NUMBERS PER LINE is 0, then don't write
!  line numbers and roles, just names.

  reportNames :−
   (form () void:
    (for string name, ref place places in namesAndPlaces()
     do (with var int count :− 0
         do writeName(name)
            (if numbersPerLine = 0
             then writeln()
             else (for int number, set roles in numbersAndRoles(places)
                   do (if roleing
                       then (for int role in elements(roles)
                             do (if count = numbersPerLine
                                 then writeln()
                                      writeBlanks(columnsPerName)
                                      count := 0)
                                writeNumberAndRole(number, role)
                                count += 1)
                       else (if count = numbersPerLine
                             then writeln()
                                  writeBlanks(columnsPerName)
                                  count := 0)
                            writeNumber(number)
                            count += 1))
                  (if count > 0
                   then writeln())))))

!  NAMES AND PLACES. Iterator. Visit every NAME string, and its linked chain of
!  PLACEs, in ROOT's subtrees. NAME strings are visited in lexicographic order.

  namesAndPlaces :−
   (form () foj:
    (form (form (string, ref place) obj body) obj:
     (with

!  NAMING AND PLACING. Do all the work for NAMES AND PLACES. We simply traverse
!  ROOT in inorder.

       namingAndPlacing :−
        (proc (ref tree root) void:
         (with var ref tree subtree :− root
          do (while subtree ≠ nil
              do (with
                   string name :− subtree↑.name
                   ref place where :− subtree↑.first
                  do namingAndPlacing(subtree↑.left)
                     body(name, where)
                     subtree := subtree↑.right))))

!  This is NAMES AND PLACES's body. The NAME at ROOT is an empty string, so its
!  LEFT subtree is empty.

      do namingAndPlacing(root↑.right))))

!  NUMBERS AND ROLES. Iterator. Visit each NUMBER and ROLE in FIRST, a chain of
!  PLACEs.

  numbersAndRoles :−
   (form (ref place first) foj:
    (form (form (int, set) obj body) obj:
     (with var ref place where :− first
      do (while where ≠ nil
          do (with
               int number :− where↑.number
               set roles  :− where↑.roles
              do body(number, roles)
                 where := where↑.next)))))

!  WRITE BLANKS. Write COUNT blanks.

  writeBlanks :−
   (form (inj count) void:
    (in count
     do write(' ')))

!  WRITE NAME. Write NAME left justified, and blank filled, in COLUMNS PER NAME
!  chars.

  writeName :−
   (form (string name) void:
    (with var int columns :− 0
     do (for breaker() break, char ch in elements(name)
         do (if asciiing
             then (if columns + 1 > columnsPerName
                   then break()
                   else columns += 1
                        write((if isAscii(ch) then ch{char0} else '_')))
             else (with int temp :− width(ch)
                   do (case temp
                       of −1: (if columns + 1 > columnsPerName
                               then break()
                               else columns += 1
                                    write(''□''))
                           0: (if accenting
                               then write(ch)
                               else if columns + 1 > columnsPerName
                                    then break()
                                    else columns += 1
                                         write(' ')
                                         write(ch))
                        none: (if columns + temp > columnsPerName
                               then break()
                               else columns += temp
                                    write(ch))))))
        (if numbersPerLine > 0
         then writeBlanks(columnsPerName − columns))))

!  WRITE NUMBER. Write a line NUMBER.

  writeNumber :−
   (form (inj number) void:
     write('' %0*i'': digitsPerNumber, number))

!  WRITE NUMBER AND ROLE. Write a line NUMBER and the character symbolizing its
!  ROLE.

  writeNumberAndRole :−
   (form (inj number, inj role) void:
     write('' %0*i%c'': digitsPerNumber, number, roleChar(role)))
)
