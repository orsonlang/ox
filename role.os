!
!  OX/ROLE. Roles and sets of roles.
!
!  Copyright © 2015 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog

!  ROLE. How a name can appear on a line.

  foj makeRole :− enum(8 × size(int1))

  inj symbolRole :− makeRole()  !  '$'  Name in a symbol type.
  inj boldRole   :− makeRole()  !  'b'  Bold name.
  inj callRole   :− makeRole()  !  'c'  Name called as a method.
  inj equateRole :− makeRole()  !  'e'  Name defined in an equate.
  inj formRole   :− makeRole()  !  'f'  Name as a form parameter.
  inj genRole    :− makeRole()  !  'g'  Name as a generic parameter.
  inj hookRole   :− makeRole()  !  'h'  Name in a hook.
  inj leftRole   :− makeRole()  !  'l'  Name as a (left) prefix operator.
  inj middleRole :− makeRole()  !  'm'  Name as a (middle) infix operator.
  inj procRole   :− makeRole()  !  'p'  Name as a procedure parameter.
  inj pastRole   :− makeRole()  !  'q'  Name in a past-clause.
  inj rightRole  :− makeRole()  !  'r'  Name as a (right) postfix operator.
  inj slotRole   :− makeRole()  !  's'  Name as a slot.
  inj tupleRole  :− makeRole()  !  't'  Name as a tuple slot.
  inj unitRole   :− makeRole()  !  'u'  Name as a unit.
  inj wrapRole   :− makeRole()  !  'w'  Name as a wrapper parameter.

  inj minRole    :− symbolRole  !  Minimum ROLE.
  inj maxRole    :− wrapRole    !  Maximum ROLE.

!  SET. A set of ROLEs.

  set :− int1

!  ELEMENTS. Iterator. Call BODY on each element of ROLES.

  elements :−
   (form (set roles) foj:
    (form (form (int) obj body) obj:
     (for int role in minRole, maxRole
      do (if role ∊ roles
          then body(role)))))

!  MAKE SET. Return a new SET whose element is ROLE.

  makeSet :−
   (form (inj role) set:
     1{int1} ← role)

!  ROLE CHAR. Return a CHAR0 that identifies ROLE.

  roleChar :−
   (form (inj role) char0:
    (case role
     of symbolRole: '$'
          boldRole: 'b'
          callRole: 'c'
        equateRole: 'e'
          formRole: 'f'
           genRole: 'g'
          hookRole: 'h'
          leftRole: 'l'
        middleRole: 'm'
          procRole: 'p'
          pastRole: 'q'
         rightRole: 'r'
          slotRole: 's'
         tupleRole: 't'
          unitRole: 'u'
          wrapRole: 'w'
              none: '?'))

!  "∊". Test if ROLE is an element of ROLES.

  "∊" :−
   (form (inj role, set roles) bool:
     makeSet(role) & roles ≠ 0)

!  "∪=". Add the elements of NEW ROLES to OLD ROLES.

  "∪=" :−
   (form (var set oldRoles, set newRoles) void:
     oldRoles |= newRoles)
)
