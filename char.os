!
!  OX/CHAR. Read characters from a source file.
!
!  Copyright © 2015 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog
  var char   ch                       !  Last char from SOURCE.
  char       eos :− (past eos){char}  !  End of source file.
  var stream source                   !  Source file stream.
  var int    sourceLine               !  Line number from SOURCE.
  var int    sourcesLine :− 0         !  Line number from all SOURCEs.
  var string sourcePath               !  Pathname of SOURCE's file.
  var int    sourceState              !  Execution state of NEXT CHAR.

!  NEXT CHAR. Advance CH to the next char in SOURCE, writing chars to OUTPUT as
!  we go. We use a 3-state DFA to control how source files are listed.

  nextChar :−
   (proc () void:
    (with

!  WRITE CHAR. Write CH to OUTPUT.

      writeChar :−
       (form () void:
        (if ¬ accenting ∧ width(ch) = 0
         then write(' '))
        (if asciiing ∧ ¬ isAscii(ch)
         then write('_')
         else write(ch)))

!  WRITE LINE NUMBER. Write the SOURCE LINE number to OUTPUT.

      writeLineNumber :−
       (form () void:
         sourceLine += 1
         sourcesLine += 1
         write(''%0*i'': digitsPerNumber, sourcesLine))

!  This is NEXT CHAR's body.

     do (case sourceState
         of 0: (case ch := read(source); ch
                of eol: (writeLineNumber()
                         writeln())
                   eos: (sourceState := 2)
                  none: (writeLineNumber()
                         write(' ')
                         writeChar()
                         sourceState := 1))
            1: (case ch := read(source); ch
                of eol: (writeln()
                         sourceState := 0)
                   eos: (writeln()
                         sourceState := 2)
                  none: (writeChar())))))

!  SYNTAX ERROR. Terminate Ox with a message about a syntax error.

  syntaxError :−
   (form () void:
     flush(output)
     fail(''Syntax error in '%s' at line %05i.'': sourcePath, sourceLine))
)
