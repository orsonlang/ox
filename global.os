!
!  OX/GLOBAL. Global declarations.
!
!  Copyright © 2016 James B. Moen.
!
!  This  program is free  software: you  can redistribute  it and/or  modify it
!  under the terms  of the GNU General Public License as  published by the Free
!  Software Foundation,  either version 3 of  the License, or  (at your option)
!  any later version.
!
!  This program is distributed in the  hope that it will be useful, but WITHOUT
!  ANY  WARRANTY;  without even  the  implied  warranty  of MERCHANTABILITY  or
!  FITNESS FOR  A PARTICULAR PURPOSE.  See  the GNU General  Public License for
!  more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!

(prog
  inj      maxNameLength   :− 1024   !  Max CHAR0s per name.
  var bool accenting       :− false  !  (-z) Overprint accents?
  var bool asciiing        :− false  !  (-a) Write chars in ASCII?
  var int  columnsPerName  :− 32     !  (-c) Columns per name.
  var int  digitsPerNumber :− 5      !  (-d) Digits per line number.
  var int  numbersPerLine  :− 7      !  (-n) Line numbers per line.
  var bool roleing         :− true   !  (-l) Line numbers only?
)
